# My Ultimate Moon .zshrc

# Oh-my-posh prompt
eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/pure.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/1_shell.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/tokyonight_storm.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/half-life.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/honukai.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/illusi0n.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/kali.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/nordtron.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/onehalf.minimal.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/powerlevel10k_lean.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/peru.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/sim-web.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/tokyo.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/uew.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/wopian.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/ys.omp.json)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/mytheme.omp.toml)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/cat.omp.toml)"
# eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/themes/catppuccin_mocha.omp.json)"

# Starship prompt
# export STARSHIP_CONFIG=~/.config/starship.toml
# export STARSHIP_CACHE=~/.starship/cache
# eval "$(starship init zsh)"
#:NOTE either you uncomment pure or the starship cuz uncommenting both will cause issues
# Pure prompt
# fpath+=($HOME/.zsh/pure)
# autoload -U promptinit; promptinit
# prompt pure

# Decorations
figlet Python Guru | lolcat
# colorscript -r
# colorscript -r | lolcat
# figlet Python Guru

# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add in zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions

# Add in snippets
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::ubuntu
zinit snippet OMZP::command-not-found

# Load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

# Keybindings
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
bindkey '^[w' kill-region
bindkey "^[[3~" delete-char

# History
HISTSIZE=10000
HISTFILESIZE=10000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no

# My Aliases
# alias ls='ls --color'
alias ls='lsd'
alias pip='pip3'
alias vim='nvim'
alias c='clear'
alias nvz='nvim ~/.zshrc'
alias xcopy='xsel --input --clipboard'
alias xpaste='xsel --output --clipboard'
alias ff='fastfetch'
alias cmatrix='cmatrix -C blue'
alias unimatrix='unimatrix -n -s 96 -l o -c blue'
# alias ydlF='youtube-dl -F'
alias ydlF='yt-dlp -F'
# alias ydlf='youtube-dl -f'
alias ydlf='yt-dlp -f'
alias tyc='tty-clock -c -s -t'
alias irs='redshift -P -O 3200k'
alias sha='sha2546sum'
alias sha2='shasum -a 256'
alias ch='cht.sh --shell'
# alias yt-dl-mp3='youtube-dl --extract-audio --audio-format mp3'
alias yt-dl-mp3='yt-dlp --extract-audio --audio-format mp3'
# alias ll='ls -latr'
alias ll='lsd -latr'
alias python='python3'
alias py='python3'
alias code="codium"
alias codes="codium-insiders"
alias i3lock="i3lock-fancy"
# alias 0101='setxkbmap -layout us'
# alias 0102='setxkbmap -layout ar'
# alias 0103='setxkbmap -layout de'
# alias 0104='setxkbmap -layout rs'

# Shell integrations
eval "$(zoxide init --cmd cd zsh)"

# Exports

# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init --path)"
# eval "$(pyenv virtualenv-init -)"

export PATH="$PATH:/opt/nvim-linux64/bin"

export PATH="$PATH:/home/moon/.cargo/bin"

export PATH="$PATH:/home/moon/.local/bin"

export SUDO_EDITOR='nvim'
export VISUAL='nvim'

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# FZF-TAB 
zinit light Aloxaf/fzf-tab

zinit snippet OMZP::fzf

zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

eval "$(fzf --zsh)"

bindkey "^[[3~" delete-char

set -o vi