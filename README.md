* Make sure zoxide and fzf are latest version and copy the fzf bin to /usr/bin/fzf and same goes for zoxide.
* Install both from their github repos not from the linux distro repo unless you are using archlinux.
* Inorder for the fzf-tab to work it need to be the last thing to be loaded.
